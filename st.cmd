#!/usr/bin/env iocsh.bash

################################################################
### requires
require(lakeshore336)

### lakeshore336 (ls1)
epicsEnvSet("IPADDR",               "192.168.2.44")
epicsEnvSet("IPPORT",               "7777")
epicsEnvSet("LOCATION",             "V20: $(IPADDR)")
epicsEnvSet("SYS",                  "SES-HGNSANS-01:")
epicsEnvSet("DEV",                  "Tctrl-LS336-003")
epicsEnvSet("PREFIX",               "$(SYS)$(DEV)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(lakeshore336_DIR)db/")

### E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

### load all db's
iocshLoad("$(lakeshore336_DIR)lakeshore336.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

### install SNL curves
seq install_curve, "P=$(PREFIX), CurvePrefix=File"

iocInit()
